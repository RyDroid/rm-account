# Script to remove account through web browser

## Why could it be useful?

- In some cases, account deletion only transform the user in a ghost.
  But you may want to go further: remove everything.
- When an account is said to be removed, it may just be hidden.
  Like this, entities make it like it is deleted,
  but it is still possible to use the data.
  Of course, it may be done for everything and not just the account,
  but there is less probability for this.
- Removing each thing of an account can be a very huge work by hand,
  that is why we created an automated tool.
- We made it to remove everything.
  This does not imply that it effectively does it, but it is the aim.
  You may be interested to only remove some parts,
  however we have no will to implement this,
  but you can fork it to mutate it for your goal.

## What you need

### Debian GNU/Linux and Trisquel GNU/Linux

`aptitude install python3-selenium chromium-driver`

## Similar (free/libre) tools

- https://forget.codl.fr/about/
- https://github.com/ThomasLeister/mastopurge

## Licensing

It is free/libre software, with the definition of Richard Stallman.
Of course, it does not depend on proprietary code.
