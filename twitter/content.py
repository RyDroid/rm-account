# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


from time import sleep

from generic.content import GenericContent


class TwitterPersonalContent(GenericContent):

    def remove(self):
        sleep(1)
        more_xpath = (
            './/div[@role="button" and'
            '       (@aria-label="plus" or @aria-label="Plus" or'
            '        @aria-label="more" or @aria-label="More")')
        more_element = self.element.find_element_by_xpath(
            more_xpath)
        more_element.click()
        sleep(1)
        remove_xpath = (
            '//*[contains(., "supprimer") or'
            '    contains(., "Supprimer") or'
            '    contains(., "remove") or'
            '    contains(., "Remove") or'
            '    contains(., "delete") or'
            '    contains(., "Delete") or'
            '    contains(., "erase") or'
            '    contains(., "Erase")]')
        remove_element = self.element.find_element_by_xpath(
            remove_xpath)
        remove_element.click()


class TwitterRetweetContent(GenericContent):

    def remove(self):
        sleep(1)
        self.element.click()
        sleep(1)
        remove_xpath = (
            '//*[contains(., "annuler le retweet") or'
            '    contains(., "Annuler le retweet") or'
            '    contains(., "Annuler le Retweet")]')
        remove_element = self.browser.find_element_by_xpath(
            remove_xpath)
        remove_element.click()
