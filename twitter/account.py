# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


from sys import stderr
from time import sleep

from selenium.common.exceptions import NoSuchElementException

from generic.account import GenericAccount
from twitter.content import (
    TwitterPersonalContent,
    TwitterRetweetContent,
)


class TwitterAccount(GenericAccount):
    URLS = (
        "https://twitter.com/",
    )

    def login(self):
        self.browser.get(self.url + '/login')
        sleep(1)

        login_field = self.browser.find_element_by_xpath(
            '//form//div[.//*[contains(., "username")]]'
            '//input[contains(@name, "user")]')
        if not login_field:
            print("No login field found!",
                  file=stderr)
            return False
        login_field.send_keys(self.login_id)

        sleep(1)

        password_field = self.browser.find_element_by_xpath(
            '//form//div[.//*[contains(., "pass") or contains(., "Pass")]]'
            '//input[contains(@name, "pass")]')
        if not password_field:
            print("No password field found!",
                  file=stderr)
            return False
        password_field.send_keys(self.password)

        sleep(1)

        login_button = self.browser.find_element_by_xpath(
            '//form//div[@role = "button"]'
            '//*[contains(., "log in") or contains(., "Log in")]')
        if not login_button:
            print("No login button found!",
                  file=stderr)
            return False
        login_button.click()

        return True

    def get_contents(self):
        self.browser.get(self.url + '/' + self.login_id)
        sleep(1)

        personal_post_xpath = (
            '//div[@data-testid="tweet"]'
            '//div[.//a[contains(@href, "/{:s}")] and'
            '      .//div[@role="button" and'
            '             (@aria-label="plus" or @aria-label="Plus" or'
            '              @aria-label="more" or @aria-label="More")]]'
            ''.format(self.login_id))
        while True:
            self.browser.refresh()
            try:
                personal_post_element = self.browser.find_element_by_xpath(
                    personal_post_xpath)
                if personal_post_element:
                    yield TwitterPersonalContent(
                        self.browser, personal_post_element)
                else:
                    break
            except NoSuchElementException:
                break

        retweet_xpath = (
            '//div[contains(@aria-label, "retweeté") or'
            '      contains(@aria-label, "Retweeté")]')
        while True:
            self.browser.refresh()
            try:
                retweet_element = self.browser.find_element_by_xpath(
                    retweet_xpath)
                if retweet_element:
                    yield TwitterRetweetContent(
                        self.browser, retweet_element)
                else:
                    break
            except NoSuchElementException:
                break
