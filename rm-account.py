#!/usr/bin/python3

# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


from sys import argv, stderr, exit
from time import sleep

from selenium import webdriver

from generic.proxy import Proxy


if len(argv) != 4:
    print("3 arguments needed and not more.\n"
          "1. URL ; 2. login ; 3. password.",
          file=stderr)
    exit(1)
url      = argv[1]
login    = argv[2]
password = argv[3]

options = webdriver.ChromeOptions()
options.add_argument('incognito')
options.add_argument('lang=en')
options.add_argument('--disable-notifications')
options.add_argument('headless')
browser = webdriver.Chrome(options=options)

try:
    proxy = Proxy(url)
    account = proxy.get_account_instance(browser, login, password)
    if account is None:
        print("No account managed for given parameters.",
              file=stderr)
    else:
        account.set_url(url)
        account.login()
        sleep(1)
        nb = account.remove_contents()
        print("{:d} removed.".format(nb))
        browser.get_screenshot_as_file("end.png")
finally:
    browser.quit()
