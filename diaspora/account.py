# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


from sys import stderr
from time import sleep

from generic.account import GenericAccount
from diaspora.content import (
    DiasporaPersonalPost,
    DiasporaPersonalComment,
    DiasporaReshareContent,
    DiasporaWatchedContent,
)


class DiasporaAccount(GenericAccount):
    URLS = (
        "https://framasphere.org/",
        "https://diaspora-fr.org/",
        "https://parlote.facil.services/",
        "https://diaspora.psyco.fr/",
    )

    def login(self):
        self.browser.get(self.url.rstrip('/') + '/users/sign_in')
        sleep(1)

        login_field = self.browser.find_element_by_css_selector(
            '#login form input#user_username')
        if not login_field:
            print("No login field found!",
                  file=stderr)
            return False
        login_field.send_keys(self.login_id)

        sleep(1)

        password_field = self.browser.find_element_by_css_selector(
            '#login form input#user_password')
        if not password_field:
            print("No password field found!",
                  file=stderr)
            return False
        password_field.send_keys(self.password)

        sleep(1)

        login_button = self.browser.find_element_by_css_selector(
            '#login form *[type="submit"]')
        if not login_button:
            print("No login button found!",
                  file=stderr)
            return False
        login_button.click()

        return True

    def get_a_personal_post_already_there(self, index=0):
        XPATH = (
            '/html/body'
            '//div[@id="main-stream"]//div[contains(@class, "stream-element")]'
            '[.//i[contains(@class, "entypo-trash")'
            ' and not(@data-template="reshare")]]')
        element = self.browser.find_element_by_xpath(
            '({:s})[{:d}]'.format(XPATH, index + 1))
        if not element:
            return None
        return DiasporaPersonalPost(self.browser, element)

    def get_a_personal_reshare_already_there(self, index=0):
        XPATH = (
            '/html/body'
            '//div[@id="main-stream"]//div[contains(@class, "stream-element")]'
            '[.//i[contains(@class, "entypo-trash")'
            ' and @data-template="reshare"]]')
        element = self.browser.find_element_by_xpath(
            '({:s})[{:d}]'.format(XPATH, index + 1))
        if not element:
            return None
        return DiasporaReshareContent(self.browser, element)

    def get_a_watched_content_already_there(self, index=0):
        XPATH = (
            '/html/body'
            '//div[@id="main-stream"]//div[contains(@class, "stream-element")]'
            '[.//*[contains(@class, "destroy_participation")]]')
        element = self.browser.find_element_by_xpath(
            '({:s})[{:d}]'.format(XPATH, index + 1))
        if not element:
            return None
        return DiasporaWatchedContent(self.browser, element)

    def get_a_personal_comment_already_there(self, index=0):
        XPATH = (
            '/html/body'
            '//div[@id="main-stream"]//div[contains(@class, "stream-element")]'
            '//div[contains(@class, "comments")]'
            '//div[contains(@class, "comment") and'
            '      ./a[contains(@class, "delete")]]')
        element = self.browser.find_element_by_xpath(
            '({:s})[{:d}]'.format(XPATH, index + 1))
        if not element:
            return None
        return DiasporaPersonalComment(self.browser, element)

    def get_personal_contents_already_there(self):
        for comment in self.get_personal_comments_already_there():
            yield comment

        for post in self.get_personal_posts_already_there():
            yield post

        for reshare in self.get_personal_reshares_already_there():
            yield reshare

    def get_contents_already_there(self):
        for personal_content in self.get_personal_contents_already_there():
            yield personal_content

        for watched in self.get_watched_contents_already_there():
            yield watched

    def get_contents(self):
        self.browser.get(self.url.rstrip('/') + '/activity')
        sleep(1)

        element = self.browser.find_element_by_css_selector(
            '.alert-dismissible')
        if element:
            element = element.find_element_by_css_selector(
                '.close')
            element.click()

        go_on = True
        while go_on:
            go_on = False
            for i in range(20):
                self.browser.execute_script(
                    "window.scrollTo(0, document.body.scrollHeight)")
                sleep(4)
                contents = list(self.get_contents_already_there())
                for content in contents:
                    go_on = True
                    yield content
            self.browser.refresh()
