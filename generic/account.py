# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


from selenium.common.exceptions import NoSuchElementException


class GenericAccount:

    def __init__(self, browser, login: str, password: str):
        self.browser  = browser
        self.login_id = login
        self.password = password

    def set_url(self, url: str):
        self.url = url

    def get_contents_already_there_from_function(self, a_function):
        index = 0
        while True:
            try:
                content = a_function(index)
                if content:
                    yield content
                    index += 1
                else:
                    break
            except NoSuchElementException:
                break

    def get_personal_posts_already_there(self):
        return self.get_contents_already_there_from_function(
            self.get_a_personal_post_already_there)

    def get_personal_reshares_already_there(self):
        return self.get_contents_already_there_from_function(
            self.get_a_personal_reshare_already_there)

    def get_personal_comments_already_there(self):
        return self.get_contents_already_there_from_function(
            self.get_a_personal_comment_already_there)

    def get_watched_contents_already_there(self):
        return self.get_contents_already_there_from_function(
            self.get_a_watched_content_already_there)

    def get_contents_already_there(self):
        raise NotImplementedError

    def get_contents(self):
        raise NotImplementedError

    def remove_contents(self, nb_max_followed_errors=10) -> int:
        nb_removed = 0
        nb_followed_errors = 0
        for content in self.get_contents():
            try:
                if content.remove():
                    nb_removed += 1
                    nb_followed_errors = 0
                else:
                    nb_followed_errors += 1
                    if nb_followed_errors >= nb_max_followed_errors:
                        break
            except:
                nb_followed_errors += 1
                if nb_followed_errors >= nb_max_followed_errors:
                    break
        return nb_removed

    def remove_account(self):
        raise NotImplementedError

    def remove_fully(self):
        self.remove_contents()
        self.remove_account()
