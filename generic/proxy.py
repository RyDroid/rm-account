# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


from generic.account import GenericAccount
from diaspora.account import DiasporaAccount
from mastodon.account import MastodonAccount
from twitter.account import TwitterAccount


ACCOUNT_TYPES = (
    DiasporaAccount,
    MastodonAccount,
    TwitterAccount,
)


class Proxy:

    def __init__(self, url: str):
        self.url = url

    def get_account_type(self):
        for account_type in ACCOUNT_TYPES:
            for url in account_type.URLS:
                if url == self.url:
                    return account_type
        return None

    def get_account_instance(
            self, browser, login: str, password: str) -> GenericAccount:
        account_type = self.get_account_type()
        if account_type is None:
            return None
        return account_type(browser, login, password)
