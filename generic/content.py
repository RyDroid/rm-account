# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


from sys import stderr
import traceback
import pdb

from selenium.common.exceptions import (
    ElementClickInterceptedException,
    ElementNotInteractableException,
)


class GenericContent:

    def __init__(self, browser, element):
        self.browser = browser
        self.element = element

    def scroll_to_given_element(self, element):
        self.browser.execute_script("arguments[0].scrollIntoView();", element)

    def click_to_given_element(self, element):
        self.scroll_to_given_element(element)
        try:
            element.click()
        except ElementClickInterceptedException:
            # to avoid potential navigation bar
            self.browser.execute_script("window.scrollBy(0, -49)")
            try:
                element.click()
            except ElementClickInterceptedException:
                # to avoid potential footer
                self.browser.execute_script("window.scrollBy(0, 98)")
                try:
                    element.click()
                except ElementClickInterceptedException:
                    print("ElementClickInterceptedException!",
                          file=stderr)
                    print(traceback.format_exc())
                    pdb.set_trace()
                    pass
        except ElementNotInteractableException:
            print("ElementNotInteractableException!",
                  file=stderr)
            print(traceback.format_exc())
            pdb.set_trace()
            pass

    def remove(self) -> bool:
        raise NotImplementedError
