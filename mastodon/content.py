# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


from sys import stderr
from time import sleep

from generic.content import GenericContent


class MastodonPersonalPost(GenericContent):
    pass


class MastodonReshareContent(GenericContent):

    def remove(self):
        element = self.element.find_element_by_css_selector(
            'button[aria-pressed="true"]')
        if not element:
            print("No delete button found!",
                  file=stderr)
            return False
        self.click_to_given_element(element)
        sleep(12)
        return True
