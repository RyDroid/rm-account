# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


from sys import stderr
from time import sleep

from generic.account import GenericAccount
from mastodon.content import (
    MastodonPersonalPost,
    MastodonReshareContent,
)


class MastodonAccount(GenericAccount):
    URLS = (
        "https://mamot.fr/",
        "https://mastodon.social/",
        "https://framapiaf.org/",
        "https://todon.eu/",
        "https://mastodon.zaclys.com/",
        "https://mastodon.tetaneutral.net/",
        "https://mastodon.gougere.fr/",
        "https://social.logilab.org/",
    )

    def login(self):
        self.browser.get(self.url.rstrip('/') + '/auth/sign_in')
        sleep(1)

        login_field = self.browser.find_element_by_css_selector(
            'form input#user_email')
        if not login_field:
            print("No login field found!",
                  file=stderr)
            return False
        login_field.send_keys(self.login_id)

        sleep(1)

        password_field = self.browser.find_element_by_css_selector(
            'form input#user_password')
        if not password_field:
            print("No password field found!",
                  file=stderr)
            return False
        password_field.send_keys(self.password)

        sleep(1)

        login_button = self.browser.find_element_by_css_selector(
            'form .actions *[type="submit"]')
        if not login_button:
            print("No login button found!",
                  file=stderr)
            return False
        login_button.click()

        return True

    def get_a_personal_post_already_there(self, index=0):
        element = self.browser.find_element_by_xpath(
            '/html/body//TODO')
        if not element:
            return None
        return MastodonPersonalPost(self.browser, element)

    def get_a_personal_reshare_already_there(self, index=0):
        XPATH = (
            '//html/body'
            '//*[@id="mastodon"]//*[contains(@class, "columns-area")]'
            '//*[contains(@class, "column") and @role="region"][3]'
            '//article[.//button[@aria-pressed="true"]]')
        xpath_indexed = '({:s})[{:d}]'.format(XPATH, index + 1)
        element = self.browser.find_element_by_xpath(xpath_indexed)
        if not element:
            return None
        return MastodonReshareContent(self.browser, element)

    def get_personal_contents_already_there(self, index=0):
        for post in self.get_personal_posts_already_there():
            yield post

        for reshare in self.get_personal_reshares_already_there():
            yield reshare

    def get_contents_already_there(self):
        return self.get_personal_contents_already_there()

    def get_contents(self):
        go_on = True
        while go_on:
            element = self.browser.find_element_by_css_selector(
                '.navigation-bar .permalink')
            element.click()
            sleep(4)

            go_on = False
            contents = list(self.get_contents_already_there())
            for content in contents:
                go_on = True
                yield content

            if go_on and next(self.get_contents_already_there()):
                self.browser.refresh()
